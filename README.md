## How to Deploy
Install a recent version of capistrano (e.g., `gem install capistrano capistrano-ruby`
Ensure that you have a pubic ssh key installed on the deployment server under the deployment user.  This is `www.myvontcounts.app` and `/home/deployer/.ssh/authorized_keys` for the canonical production server.
Then,
```bash
cap production deploy`
```

## Creator

Start Bootstrap was created by and is maintained by **[David Miller](http://davidmiller.io/)**, Owner of [Blackrock Digital](http://blackrockdigital.io/).

* https://twitter.com/davidmillerskt
* https://github.com/davidtmiller

Start Bootstrap is based on the [Bootstrap](http://getbootstrap.com/) framework created by [Mark Otto](https://twitter.com/mdo) and [Jacob Thorton](https://twitter.com/fat).

## Copyright and License

Copyright 2013-2016 Blackrock Digital LLC. Code released under the [MIT](https://github.com/BlackrockDigital/startbootstrap-new-age/blob/gh-pages/LICENSE) license.
