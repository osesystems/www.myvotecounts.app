set :application, 'www.myvotecounts.app'
set :repo_url, 'git@gitlab.com:osesystems/www.myvotecounts.app.git'
set :branch, `git rev-parse --abbrev-ref HEAD`.chomp
set :deploy_to, '/www/sites/www.myvotecounts.app'
set :scm, :git
set :keep_releases, 2
